<?php
require_once dirname(__FILE__) . "/library/DB.php";
require_once dirname(__FILE__) . "/library/Validate.php";
require_once dirname(__FILE__) . "/config/config.php";
session_start();
$db = new DB();
$db->db_connect();

$valid = new Validate();

if($_POST) {

	$data = $_POST['user'];

	//validate
	$rules = array(
		'user[firstname]' => array(
				'label'	=> 'firstname',
				'required' => true,
			),
		'user[lastname]' => array(
				'label'	=> 'lastname',
				'required' => true,
			),
		'user[birthday]' => array(
				'label'	=> 'birthday',
				'required' => true,
			),
		'user[username]' => array(
				'label'	=> 'username',
				'required' => true,
			),
		'user[password]' => array(
				'label'	=> 'password',
				'required' => true,
			),
		're_password' => array(
				'label'	=> 're password',
				'compare' => 'user[password]',
			),
	);

	if($valid->run($rules))	 {
		$sql = "INSERT INTO user (" . implode(", ", array_keys($data)) . ") VALUES ('" . implode("', '", array_values($data) ). "')";
		$db->db_query($sql);
		header("Location: login.php?regis=success");
	}

	$errors = $valid->errors;
}

?>

<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" media="all" href="/css/style.css">
<body>
<div class="wrapper">
<h1>SignUp!</h1>
	<form name="thanks" action="signup.php" method="post">
		<p><b>personal info</b></p>
		<table>
			<tr>
				<td><label>First name</label></td>
				<td><input type="text" name="user[firstname]"><?=$valid->error_message('user[firstname]');?></td>
			</tr>
			<tr>
				<td><label>Last name</label></td>
				<td><input type="text" name="user[lastname]"><?=$valid->error_message('user[lastname]');?></td>
			</tr>
			<tr>
				<td><label>Birthday</label></td>
				<td><input type="text" name="user[birthday]"><?=$valid->error_message('user[birthday]');?></td>
			</tr>
		</table>
		<p><b>login info</b></p>
		<table>
			<tr>
				<td><label>Login name</label></td>
				<td><input type="text" name="user[username]"><?=$valid->error_message('user[username]');?></td>
			</tr>
			<tr>
				<td><label>Password</label></td>
				<td><input type="text" name="user[password]"><?=$valid->error_message('user[password]');?></td>
			</tr>
			<tr>
				<td><label>Re-password</label></td>
				<td><input type="text" name="re_password"><?=$valid->error_message('re_password');?></td>
			</tr>
		</table>
		<p><input type="submit" value="signup"></p>
	</form>
</div>
</body>
</html>