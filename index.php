<?php
require_once dirname(__FILE__) . "/library/DB.php";
require_once dirname(__FILE__) . "/config/config.php";
session_start();
$db = new DB();
$db->db_connect();

$sql = "SELECT * FROM article a LEFT JOIN user u ON `a`.`user_id` = `u`.id WHERE 1 ORDER BY created";

$rs = $db->db_query($sql);
$articles = $db->fetchAll($rs);

?>

<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" media="all" href="/css/style.css">
<body>
<div class="wrapper">
<h1>Welcome to X </h1>
<a href="/login.php">Login</a> | <a href="/signup.php">Signup</a>

<p>All Article</p>
<?php 
		foreach ($articles as $article) { ?>
		<div class="rows">
			<p class="title"><?=$article['title']?></p>
			<p class="author"><?=$article['username']?></p>
			<p class="content"><?=$article['content']?></p>
		</div>
		<?php 
		}
	?>

</div>
</body>
</html>


