<?php
require_once dirname(__FILE__) . "/library/DB.php";
require_once dirname(__FILE__) . "/config/config.php";
session_start();
if(!isset($_SESSION["user"])) {
	header("Location: index.php");
}
$db = new DB();
$db->db_connect();
$fday = "";
$sort = "";
if(!isset($_GET["sort"])) {
	$sort .= "ORDER BY user_eid ASC";
}
if(isset($_GET["sort"])) {
	switch ($_GET['sort']) {
		case '1':
			$sort .= "ORDER BY user_eid ASC";
			break;
		case '2':
			$sort .= "ORDER BY user_eid DESC";
			break;
		case '3':
			$sort .= "ORDER BY user_nick_name ASC";
			break;
		case '4':
			$sort .= "ORDER BY user_nick_name DESC";
			break;
		default:
			$sort .= "ORDER BY user_eid DESC";
			break;
	}
}
$filter = "";
if(isset($_GET["keyword"])) {
	$filter .= "AND (user.user_nick_name LIKE '%" . $db->db_escape_string($_GET["keyword"]) . "%' OR user.user_eid LIKE '%" . $db->db_escape_string($_GET["keyword"]) . "%') ";
}

$sql = "SELECT *, (SELECT COUNT(*) FROM comment 
					WHERE user.user_id = comment.user_id 
						GROUP BY user_id) AS thanks , 
				  (SELECT COUNT(*) FROM comment 
					WHERE user.user_id = comment.user_id AND comment_reg_datetime BETWEEN DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() 
						GROUP BY user_id) AS thank_in_month ,
				  (SELECT comment_content FROM comment
				  	WHERE user.user_id = comment.user_id
				  		ORDER BY comment_reg_datetime DESC LIMIT 1) AS last_comment
							FROM user WHERE active = '1' " . $filter . $sort;
							
$rs = $db->db_query($sql);
$users = $db->fetchAll($rs);

$content = file_get_contents("content.htm");
$regex = "/\/user\/detail\?id=([0-9]+)\">([^<]*?)</";
preg_match_all($regex, $content, $matches);
?>

<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<body>
<p>Hi! <?php echo $_SESSION["user"]["user_nick_name"] ?> | <a href="logout.php">Logout</a></p>
<h1>CBA Members</h1>

<form method="get" action="list.php">
	<label>Filter:</label>
	<input type="text" name="keyword" />
</form>
<?php if (!empty($users)) { ?>
<p>List members</p>
<div style="float:left;width:50%">
<table border="1" cellpadding="3" width="98%">
	<thead>
		<th>EID <a href="list.php?sort=1">▲</a><a href="list.php?sort=2">▼</a></th>
		<th>Image</th>
		<th>Nick Name <a href="list.php?sort=3">▲</a><a href="list.php?sort=4">▼</a></th>
		<th>Last Comment</th>
		<th>Thanks</th>		
	</thead>
	<tbody>
		<?php
			for ($i=0; $i < count($users) ; $i++) {
				if (($i%2)==0) {

					$img = "<img src='https://group.cybridge.jp/img/user/user_{$users[$i]['user_gwid']}.jpg' width='40' height='55' />";
					
					echo "<tr>";
						echo "<td>{$users[$i]['user_eid']}</td>";
						echo "<td>$img</td>";
						echo "<td>{$users[$i]['user_nick_name']}</td>";
						echo "<td>{$users[$i]['last_comment']}";
						if($users[$i]['user_id'] == $_SESSION['user']['user_id']) {
							echo "<br><a href='whothank.php'>Persons who was say 'thank' you.<a/>";
						}
						echo "</td>";
						echo "<td>";
						echo "<a href='thanks.php?uid={$users[$i]['user_id']}&img={$users[$i]['user_gwid']}'>Thanks!</a></td>";
					echo "</tr>";
				}
			}
		?>
	</tbody>
</table>
</div>
<div style="float:left;width:50%">
<table border="1" cellpadding="3" width="98%">
	<thead>
		<th>EID <a href="list.php?sort=1">▲</a><a href="list.php?sort=2">▼</a></th>
		<th>Image</th>
		<th>Nick Name <a href="list.php?sort=3">▲</a><a href="list.php?sort=4">▼</a></th>
		<th>Last Comment</th>
		<th>Thanks</th>		
	</thead>
	<tbody>
		<?php
			for ($i=0; $i < count($users); $i++) { 
				if (($i%2)==1) {

					$img = "<img src='https://group.cybridge.jp/img/user/user_{$users[$i]['user_gwid']}.jpg' width='40' height='55' />";

					echo "<tr>";
						echo "<td>{$users[$i]['user_eid']}</td>";
						echo "<td>$img</td>";
						echo "<td>{$users[$i]['user_nick_name']}</td>";
						echo "<td>{$users[$i]['last_comment']}";
						if($users[$i]['user_id'] == $_SESSION['user']['user_id']) {
							echo "<br><a href='whothank.php'>Persons who was say 'thank' you.<a/>";
						}
						echo "</td>";
						echo "<td>";
						echo "<a href='thanks.php?uid={$users[$i]['user_id']}&img={$users[$i]['user_gwid']}'>Thanks!</a></td>";
					echo "</tr>";
				}
			}
		?>
	</tbody>
</table>
</div>
<?php }else { ?>
	<p>Sorry, No members found!</p>
<?php } ?>
</body>
</html>