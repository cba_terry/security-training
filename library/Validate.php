<?php
class Validate {

	public $rules;
	public $errors;

	public function run ($rules) {

		$this->rules = $rules;
		$posts = $this->convert($_POST);

		foreach ($rules as $input_name => $rule) {
			$label = $rule['label'];
			foreach ($rule as $rname => $rcond) {
				$input_value = isset($posts[$input_name])?$posts[$input_name]:'';

				if($rname != 'label') {
					call_user_func_array(array($this, $rname), array($input_value, $input_name));
				}
			}
		}

		return (count($this->errors) == 0);
	}

	private function convert($args) {
		$tmps = array();
		foreach ($args as $key => $value) {
			if(is_array($value)) {
				foreach ($value as $k => $v) {
					$tmps[$key."[" . $k . "]"] = $v;
				}
			}else {
				$tmps[$key] = $value;
			}
		}

		return $tmps;
	}

	public function required ($value, $name) {
		$label = $this->rules[$name]['label'];
		if(strlen(trim($value)) == 0){
			$this->errors[$name] = 'vui long nhập ' . $label;
		}
	}

	public function compare ($value, $name) {
		$posts = $this->convert($_POST);

		$valc = isset($posts[$this->rules[$name]['compare']])?$posts[$this->rules[$name]['compare']]:"";

		if($value !== $valc)
			$this->errors[$name] = 'not equal';
	}

	public function error_message ($input_name) {
		return isset($this->errors[$input_name])?$this->errors[$input_name]:null;
	}
}