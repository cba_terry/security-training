<?php
require_once dirname(__FILE__) . "/../library/DB.php";
require_once dirname(__FILE__) . "/../config/config.php";
session_start();
$db = new DB();
$db->db_connect();
if(!isset($_SESSION["user"])) {
	header("Location: /login.php");
}else {
	$user = $_SESSION["user"];
}

if($_POST) {
	$data = $_POST['user'];
	$errors = array();
	// validation
	if(strlen(trim($data['password'])) > 0 && $data['password'] !== $_POST['re_password']) {
		$errors[] = 'mật khẩu xác nhận không khớp';
	}

	if(count($errors) == 0) {
		$sql = "UPDATE user SET ";

		$keys = array('firstname','lastname','birthday','password');

		if(!$data['password'] || strlen(trim($data['password'])) == 0 ) {
			unset($keys[3]);
		}

		foreach ($keys as $k) {
			$vals[] = $k . " = " . "'" . $data[$k] . "'";
		}
		$sql .= implode(", ", $vals);
		$sql .= " WHERE id= '" . $data['id'] . "'";
		//echo $sql;exit();
		$db->db_query($sql);

		// update session
		$sql = "SELECT * FROM user WHERE id = '" . $user['id'] . "'";
		$rs = $db->db_query($sql);
		$user = $db->fetch_array($rs);

		$_SESSION["user"] = $user;

		header("Location: /mypage/index.php");
	}
	
}

?>

<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" media="all" href="/css/style.css">
<body>
<div class="wrapper">
<h1>Change Your Profile!</h1>

<?php if(isset($errors)) {
	foreach ($errors as $error) {
		echo "<p>{$error}</p>";
	}
}?>
	<form name="thanks" action="profile.php" method="post">
		<p><b>personal info</b></p>
		<input type="hidden" name="user[id]" value="<?=$user['id']?>">
		<table>
			<tr>
				<td><label>First name</label></td>
				<td><input type="text" name="user[firstname]" value="<?=$user['firstname']?>"></td>
			</tr>
			<tr>
				<td><label>Last name</label></td>
				<td><input type="text" name="user[lastname]" value="<?=$user['lastname']?>"></td>
			</tr>
			<tr>
				<td><label>Birthday</label></td>
				<td><input type="text" name="user[birthday]" value="<?=$user['birthday']?>"></td>
			</tr>
		</table>
		<p><b>login info</b></p>
		<table>
			<tr>
				<td><label>Password</label></td>
				<td><input type="text" name="user[password]" value=""></td>
			</tr>
			<tr>
				<td><label>Re-password</label></td>
				<td><input type="text" name="re_password"></td>
			</tr>
		</table>
		<p><input type="submit" value="signup"></p>
	</form>
</div>
</body>
</html>