<?php
require_once dirname(__FILE__) . "/../library/DB.php";
require_once dirname(__FILE__) . "/../config/config.php";
session_start();
$db = new DB();
$db->db_connect();

if(!isset($_SESSION["user"])) {
	header("Location: /login.php");
}else {
	$user = $_SESSION["user"];
}
?>

<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" media="all" href="/css/style.css">
<body>
<div class="wrapper">
<h1>Welcome <?=$user["firstname"]?><?=$user["lastname"]?></h1>
<p>This is your secret area!</p>
<p><a href="profile.php">Edit Profile</a></p>
<p><a href="article_list.php">Article</a></p>
<p><a href="/logout.php">Logout</a></p>
</div>
</body>
</html>