<?php
require_once dirname(__FILE__) . "/../library/DB.php";
require_once dirname(__FILE__) . "/../library/Validate.php";
require_once dirname(__FILE__) . "/../config/config.php";
session_start();
$db = new DB();
$db->db_connect();
if(!isset($_SESSION["user"])) {
	header("Location: /login.php");
}else {
	$user = $_SESSION["user"];
}

$_default = array();

$valid = new Validate();

if(isset($_GET['id'])) {
	$article_id = $_GET['id'];
	$sql = "SELECT * FROM article WHERE id = '" . $article_id . "'";

	$rs = $db->db_query($sql);

	$_default['article'] = $db->fetch_array($rs);
}

if($_POST) {

	$data = $_POST['article'];

	//validate
	$rules = array(
		'article[title]' => array(
				'label'	=> 'title',
				'required' => true,
			),
		'article[content]' => array(
				'label'	=> 'content',
				'required' => true,
			),
	);

	if($valid->run($rules))	 {
		if($data['id']) {

			$sql = "UPDATE article SET ";

			$keys = array('id','title','content');

			foreach ($keys as $k) {
				$vals[] = $k . " = " . "'" . $data[$k] . "'";
			}

			$sql .= implode(", ", $vals);
			$sql .= " WHERE id= '" . $data['id'] . "'";

			$db->db_query($sql);

		}else {
			$data['created'] = date("Y-m-d H:i:s");
			$sql = "INSERT INTO article (" . implode(", ", array_keys($data)) . ") VALUES ('" . implode("', '", array_values($data) ). "')";
			$db->db_query($sql);
		}

		header("Location: /mypage/article_list.php");
	}

	$errors = $valid->errors;
}

function default_value ($arr, $input) {
	foreach ($arr as $key => $value) {
		if (isset($_POST[$key][$input])) {
			return $_POST[$key][$input];
		}else if (isset($arr[$key][$input])) {
			return $arr[$key][$input];
		}
		return "";
	}
}

?>

<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" media="all" href="/css/style.css">
<body>
<div class="wrapper">
<h1>Article!</h1>
	<form name="thanks" method="post">
		<p><b>Enter a article</b></p>
		<input type="hidden" name="article[user_id]" value="<?=$user['id']?>">
		<input type="hidden" name="article[id]" value="<?=default_value($_default, 'id')?>">
		<table>
			<tr>
				<td><label>Title</label></td>
				<td><input type="text" name="article[title]" value="<?=default_value($_default, 'title')?>"><?=$valid->error_message('article[title]');?></td>
			</tr>
			<tr>
				<td><label>Content</label></td>
				<td><textarea name="article[content]" cols="50" rows="5"><?=default_value($_default, 'content')?></textarea><?=$valid->error_message('article[content]');?></td>
			</tr>
			<tr>
			<td></td><td><input type="submit" value="Post"></td>
			</tr>
		</table>
	</form>
</div>
</body>
</html>