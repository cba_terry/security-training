<?php
require_once dirname(__FILE__) . "/../library/DB.php";
require_once dirname(__FILE__) . "/../library/Validate.php";
require_once dirname(__FILE__) . "/../config/config.php";
session_start();
$db = new DB();
$db->db_connect();
if(!isset($_SESSION["user"])) {
	header("Location: /login.php");
}else {
	$user = $_SESSION["user"];
}

$sql = "SELECT * FROM article WHERE user_id = '" . $user['id'] . "'";

$rs = $db->db_query($sql);
$articles = $db->fetchAll($rs);

?>

<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" media="all" href="/css/style.css">
<body>
<div class="wrapper">
<h1>Article!</h1>
<a href="/mypage/">Mypage</a><br>
<a href="/mypage/article_edit.php">Create</a>
	<?php 
		foreach ($articles as $article) { ?>
		<div class="rows">
			<p class="title"><a href="article_edit.php?id=<?=$article['id']?>"><?=$article['title']?></a></p>
			<p class="content"><?=$article['content']?></p>
		</div>
		<?php 
		}
	?>
</div>
</body>
</html>